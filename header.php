<!DOCTYPE html>
<html lang="en">

<head>
	<title> Vinhomes Sapphire </title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/vendor/bootstrap.js"></script>

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/vendor/bootstrap.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/vendor/grid-gallery-master/css/grid-gallery.min.css">
	<link
		href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=vietnamese"
		rel="stylesheet">


	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
	<!-- <script type="text/javascript" src="vendor/zoomwall.js-master/zoomwall.js"></script> -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/1.js"></script>
    <?php wp_head();?>
</head>

<body>