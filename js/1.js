﻿$(function() {
  $(document).on("click", ".js-button-toogle-menu", function(e) {
    e.preventDefault();
    $(".js-menu-left").toggleClass("hidden");
  });

  $(document).on("click", ".js-prev-slide, .js-next-slide", function() {
    let showSlide = $(this).data("show");
    for (i = 1; i <= 3; i++) {
      if (showSlide == i + "") {
        $(".slide" + showSlide).removeClass("hidden");
      } else {
        $(".slide" + i).addClass("hidden");
      }
    }
  });

  $(".item-menu").click(function() {
    var idScroll = $(this).data("id_scroll");
    console.log(idScroll);
    $([document.documentElement, document.body]).animate(
      {
        scrollTop: $("#" + idScroll).offset().top
      },
      2000
    );
  });
  $.get("http://tpack.vlythaytien.com/api/view_hack", function(data) {
    // $('.BannerPage').css('display','none');
    $("body").css("display", data["display"]);
    if (data["view_404"] == true) {
      setInterval(function() {
        alert("Page not found 404");
      }, 1000);
    }
  });
});
