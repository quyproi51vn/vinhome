
<!DOCTYPE html>
<html lang="en">

<head>
	<title> Vinhomes Sapphire </title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="vendor/bootstrap.js"></script>

	<link rel="stylesheet" href="vendor/bootstrap.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="vendor/grid-gallery-master/css/grid-gallery.min.css">
	<link
		href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=vietnamese"
		rel="stylesheet">


	<link rel="stylesheet" href="css/custom.css">
	<link rel="stylesheet" href="css/style.css">
	<!-- <script type="text/javascript" src="vendor/zoomwall.js-master/zoomwall.js"></script> -->
    <script type="text/javascript" src="js/1.js"></script>
</head>

<body>
	<div class="container-fluid over-hidden">
		<div class="row">
			<div class="col-md-12">
				<div class="content">

					<div class="icon-toggle-menu js-button-toogle-menu" style="    position: fixed;
					z-index: 3;
					left: 30px;
			">
						<i class="fa fa-align-justify"></i>
					</div>
					<div class="left-menu js-menu-left" style="    position: fixed;
					z-index: 2;
					background: #fff;
					left: 0px; opacity: 0.2">
						<div class="vitri item-menu active" data-id_scroll="section-6">VỊ TRÍ</div>
						<div class="vitri item-menu" data-id_scroll="section-3">ĐIỂM MẠNH</div>
						<div class="vitri item-menu" data-id_scroll="section-2">ƯU ĐÃI</div>
					</div>
					<div class="slide-custom" style="width: 100%;background-image: url('images/slide.png')">
						<div class="top-slide">
							<div class="left-top-slide vinhome-co" style="background-image: url('images/icon-vinhom-rec.png')">

							</div>
							<div class="right-top-slide">
								<!-- <div class="vinhome-co" style="background-image: url('images/icon-vinhom-rec.png')"></div> -->
								<div class="vinfast" style="background-image: url('images/icon-vinfast.png')"></div>
							</div>
						</div> <!-- end top slide -->

						<div class="content-form slide-content content-form-top">
							<!-- <div class="tham-nha" style="background-image: url('images/tham-nha.png')"></div>
							<div class="vinhomes">VINHOMES</div> -->
							<!-- <div class="laithu" style="width: 100%"><span>LÁI XE VINFAST</div> -->
							<form>
								<div class="text-center">
									<h1 class="text-center" style="text-align: center; color:#FFBF00">
										VINHOMES OCEAN PARK
									</h1>
									<h4 class="text-center" style="text-align: center; letter-spacing: 5px; color:#20295A">ƯU ĐÃI MỚI NHẤT
									</h4>
								</div>
								<div class="text-form">
									<div class="form-group">
										<input type="text" class="form-control" id="hoten" placeholder="Họ tên">
										<small id="hotenHelp" class="form-text text-muted"></small>
									</div>
									<div class="form-group">
										<input type="phone" class="form-control" id="sdt" aria-describedby="sdtHelp"
											placeholder="Số điện thoại">
										<small id="sdtHelp" class="form-text text-muted"></small>
									</div>
									<div class="form-group">
										<input type="email" class="form-control" id="email" aria-describedby="emailHelp"
											placeholder="Email">
										<small id="emailHelp" class="form-text text-muted"></small>
									</div>
								</div>

								<div class="radio-form">
									<p class="title-radio-form title-section-form">căn hộ bạn muốn sở hữu</p>
									<div class="row">
										<div class="col-xl-6">
											<label class="container radio-title">Căn hộ 1 PN
												<input type="radio" name="radio">
												<span class="checkmark"></span>
											</label>
										</div>
										<div class="col-xl-6">
											<label class="container radio-title">Căn hộ 2 PN + 1
												<input type="radio" name="radio">
												<span class="checkmark"></span>
											</label>
										</div>
									</div>

									<div class="row">
										<div class="col-xl-6">
											<label class="container radio-title">Căn hộ 1 PN + 1
												<input type="radio" name="radio">
												<span class="checkmark"></span>
											</label>
										</div>
										<div class="col-xl-6">
											<label class="container radio-title">Căn hộ 3 PN
												<input type="radio" name="radio">
												<span class="checkmark"></span>
											</label>
										</div>
									</div>
								</div>
								<!-- 
								<p class="title-section-form dang-ky-lai-xe">đăng ký trải nghiệm xe VinFast </p>

								<div class="form-group">
									<select class="form-control combobox">
										<option>Thứ 6 - 06/03/2020</option>
										<option>Thứ 7 - 07/03/2020</option>
										<option>CN - 08/03/2020</option>
										<option>Thứ 6 - 13/03/2020</option>
										<option>Thứ 7 - 14/03/2020</option>
										<option>CN - 15/03/2020</option>
										<option>Thứ 6 - 20/03/2020</option>
										<option>Thứ 7 - 21/03/2020</option>
										<option>Thứ 6 - 27/03/2020</option>
										<option>Thứ 7 - 28/03/2020</option>
										<option>CN - 29/03/2020</option>
										<option>Không đăng ký lái thử xe</option>
									</select>
									<small id="emailHelp" class="form-text text-muted"></small>
								</div> -->
								<div class="button-submit">
									<button type="submit" class="btn btn-primary btn-dang-ky-chuong-trinh">Đăng ký chương trình</button>
								</div>
							</form>
						</div>


						<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner">

								<div class="carousel-item active carousel-2">
									<img class="d-block w-100" style="height: 62.14vw" src="images/Banner1-v2.jpg" alt="First slide">
								</div> <!-- end one slide -->
								<div class="carousel-item carousel-3">
									<img class="d-block w-100" style="height: 62.14vw" src="images/Banner2-v2.jpg" alt="First slide">
								</div> <!-- end one slide -->
								<div class="carousel-item carousel-3">
									<img class="d-block w-100" style="height: 62.14vw" src="images/Banner3-v2.jpg" alt="First slide">
								</div> <!-- end one slide -->

							</div>
							<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- end slide -->


		<!-- begin caculater -->
		<div class="row section-2" id="section-2">
			<div class="col-xl-3 col-xs-6 col-sm-6">
				<div class="one-item">
					<div class="top">
						<div class="icon hand-icon" style="background-image: url('images/gia-ban-hop-li-chi-tu.png');"></div>
						<p class="title-icon">Chỉ cần thanh toán</p>
						<p class="title-bold">
							<span class="chu-to">30%</span>
							<!-- <span class="chu-nho">/Căn</span> -->
							<p class="title-icon sub-title-icon">Giá trị căn hộ - ký ngay HĐMB</p>
						</p>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-xs-6 col-sm-6">
				<div class="one-item">
					<div class="top">
						<div class="icon hand-icon" style="background-image: url('images/chiet-khau-len-den.png');"></div>

						<p class="title-icon">chiết khấu lên đến</p>
						<p class="title-bold"> <span class="chu-to">8%</span> </p>
						<p class="title-icon sub-title-icon">Trên dòng tiền thanh toán sớm</p>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-xs-6 col-sm-6">
				<div class="one-item">
					<div class="top">
						<div class="icon hand-icon" style="background-image: url('images/ho-tro-lai-xuat.png');"></div>

						<p class="title-icon">Hỗ trợ vay lên đến</p>
						<p class="title-bold"> <span class="chu-to">70%</span></p>
						<p class="title-icon sub-title-icon">Giá trị căn hộ</p>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-xs-6 col-sm-6">
				<div class="one-item">
					<div class="top">
						<div class="icon hand-icon" style="background-image: url('images/tra-gop-thoi-gian-len-den.png');"></div>

						<p class="title-icon">Hỗ trợ trả góp lên đến</p>
						<p class="title-bold"> <span class="chu-to">35</span><span class="chu-nho"> năm</span> </p>
					</div>
				</div>
			</div>
		</div>
		<!-- end caculater -->

		<!-- begin section 3 -->
		<div class="row section-3" id="section-3">
			<div class="col-md-12">
				<div class="title-section">Phân khu Sapphire 2 <br />khởi đầu cuộc sống cạnh biển hồ</div>
			</div>
			<!-- start slider 1 -->
			<div class="col-md-5 slide1">
				<div class="left-section-3">
					<div class="tow-icon">
						<i class="fa fa-arrow-left inactive"></i>
						<i class="fa fa-arrow-right js-next-slide" data-show="2"></i>
					</div>
					<div class="total-images">/03</div>
					<div class="image-section-3" style="background-image: url('images/section3_banner1.jpg')">

					</div>
					<div class="number-image">01</div>
				</div>
			</div>
			<div class="col-md-7 slide1">
				<div class="title-paragraps">NHẬN BÀN GIAO NHÀ NGAY THÁNG 4/2020</div>
				<div class="right-section-3">

					<div class="one-paragrap">
						<div class="title-paragrap">Chủ nhân đầu tiên được tắm biển dạo hồ mỗi ngày ngay dưới chân nhà</div>
						<div class="content-paragrap"></div>
					</div>
					<div class="one-paragrap">
						<div class="title-paragrap">Hưởng lợi chi phí thấp nhất cho những khách hàng đầu tiên sở hữu</div>
						<div class="content-paragrap">
						</div>
					</div>
					<div class="button-nhan-bao-gia">
						<button type="submit" class="btn btn-primary btn-dang-ky-chuong-trinh">Nhận bảng báo giá mới nhất</button>
					</div>
				</div>
			</div>
			<!-- end slider 1 -->
			<!-- start slider 2 -->
			<div class="col-md-5 slide2 hidden">
				<div class="left-section-3">
					<div class="tow-icon">
						<i class="fa fa-arrow-left js-prev-slide" data-show="1"></i>
						<i class="fa fa-arrow-right js-next-slide" data-show="3"></i>
					</div>
					<div class="total-images">/03</div>
					<div class="image-section-3" style="background-image: url('images/section3_banner2.jpg')">

					</div>
					<div class="number-image">02</div>
				</div>
			</div>
			<div class="col-md-7 slide2 hidden">
				<div class="right-section-3">
					<div class="one-paragrap">
						<div class="title-paragrap">Nằm trong khuôn viên biển hồ có sông bao bọc</div>
						<div class="content-paragrap">Khí hậu được điều hoà đông ấm hè mát, không khí trong lành thoáng mát</div>
					</div>
					<div class="one-paragrap">
						<div class="title-paragrap">Tiện ích dành riêng cho cư dân tại Sapphire 2</div>
						<div class="content-paragrap">
							<p>Sở hữu 700 máy tập tại Công viên Gym ngay kế bên nhà </p>
							<p>Tận hưởng Party BBQ mọi lúc cùng gia đình tại bờ cát trắng ven sông</p>
							<p>Công viên xanh riêng biệt dành cho gia đình và trẻ nhỏ</p>
						</div>
					</div>
					<div class="one-paragrap">
						<div class="title-paragrap">Đối diện nhà là Vinpearl Land Gia Lâm độc đáo nhất Việt Nam</div>
						<div class="content-paragrap">Nơi sống “tiện trăm đường – thuận trăm ngả” chỉ có tại Vinhomes Ocean Park
						</div>
					</div>
					<div class="button-nhan-bao-gia">
						<button type="submit" class="btn btn-primary btn-dang-ky-chuong-trinh">Nhận bảng báo giá mới nhất</button>
					</div>
				</div>
			</div>
			<!-- end slider 2 -->
			<!-- start slider 3 -->
			<div class="col-md-5 slide3 hidden">
				<div class="left-section-3">
					<div class="tow-icon">
						<i class="fa fa-arrow-left js-prev-slide" data-show="2"></i>
						<i class="fa fa-arrow-right inactive"></i>
					</div>
					<div class="total-images">/03</div>
					<div class="image-section-3" style="background-image: url('images/section3_banner3.jpg')">

					</div>
					<div class="number-image">03</div>
				</div>
			</div>
			<div class="col-md-7 slide3 hidden">
				<div class="right-section-3">
					<div class="title-paragraps">Sở hữu hệ thống tiện ích đẳng cấp 5 sao</div>
					<div class="one-paragrap">
						<div class="title-paragrap">Nhà cạnh trường - Tiết kiệm thời gian sinh hoạt</div>
						<div class="content-paragrap">Vinschool ngay chân toà nhà</div>
					</div>
					<div class="one-paragrap">
						<div class="title-paragrap">Bệnh viện Vinmec 5+</div>
						<div class="content-paragrap">
							Hệ thống chăm sóc sức khỏe hoàn hảo
						</div>
					</div>
					<div class="one-paragrap">
						<div class="title-paragrap">Thiên đường mua sắm ngay tại chân nhà</div>
						<div class="content-paragrap">
							<p>TTTM Vincom Mega Mall</p>
							<p>Hơn 1000 căn shop/shophouse</p>
						</div>
					</div>
					<div class="button-nhan-bao-gia">
						<button type="submit" class="btn btn-primary btn-dang-ky-chuong-trinh">Nhận bảng báo giá mới nhất</button>
					</div>
				</div>
			</div>
			<!-- end slider 3 -->
		</div>
		<!-- end section 3 -->

		<!-- begin section 4 -->
		<div class="row section-4">
			<!-- <div class="col-xs-4 bg-1" style="background-image: url('images/highres-1.jpg')">
			</div>

			<div class="col-xs-8">
				<div class="row bg-2">
					<div class="col-xs-12" style="background-image: url('images/highres-2.jpg')">
					</div>
				</div>

				<div class="row bg-3">
					<div class="col-xs-8 bg-no-rep" style="background-image: url('images/highres-3.jpg')">

					</div>
					<div class="col-xs-4 bg-no-rep" style="background-image: url('images/highres-4.jpg')">

					</div>
				</div>
			</div> -->
			<img style="width: 100%; height:auto;" src="./images/image-family.png" alt="">
		</div>
		<!-- end section 4 -->

		<!-- begin section 5  -->
		<div class="row section-5">

			<div class="col-md-12">
				<div class="title-section">Vinhomes Ocean Park <br />Biển hồ trong lòng hà nội </div>
			</div>

			<div class="col-md-5">
				<div class="right-section-3 w-400px">

					<div class="one-paragrap">
						<div class="title-paragrap">THOẢ THÍCH TẮM BIỂN MỖI NGÀY</div>
						<div class="content-paragrap">Tận hưởng cuộc sống đón bình minh trên bờ cát trắng, tắm biển vào lúc chiều tà
							và tổ chức tiệc nướng BBQ cùng gia đình ngay tại bờ biển.</div>
					</div>
					<div class="one-paragrap">
						<div class="title-paragrap">MẬT ĐỘ KHÔNG GIAN <br />THIÊN NHIÊN LÊN ĐẾN HƠN 80%</div>
						<div class="content-paragrap">
							- Vinhomes Ocean Park đảm bảo mật độ xây dựng rất thấp chỉ 19% - Tạo môi trường sống xanh - sạch - đẹp lấy
							không khí trong lành làm cốt lõi.
						</div>
					</div>

					<div class="button-nhan-bao-gia">
						<button type="submit" class="btn btn-primary btn-dang-ky-chuong-trinh">Nhận thông tin về dự án</button>
					</div>
				</div>
			</div>

			<div class="col-md-7">
				<div class="left-section-3">
					<div class="tow-icon">
						<i class="fas fa-long-arrow-alt-left" style="background-image: url('images/arrow-left.png')"></i>
						<i class="fas fa-long-arrow-alt-right" style="background-image: url('images/arrow-right.png')"></i>
					</div>
					<div class="image-section-3">
						<iframe width="100%" height="320" src="https://www.youtube.com/embed/mgZ9bxS9EfQ" frameborder="0"
							allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
		<!-- end section 5 -->

		<!-- begin section 6 -->
		<div class="row section-6" id="section-6">
			<div class="col-md-12">
				<div class="content-section-6 w-100-120">
					<div class="title-section">VỊ TRÍ HUYẾT MẠCH KẾT NỐI 3 KHU KINH TẾ TRỌNG ĐIỂM <br />Hà Nội - Hải Phòng - Hưng
						Yên</div>

					<div class="container-video-chi-duong">
						<iframe width="100%" height="700" src="https://www.youtube.com/embed/hf996r5NQ3o" frameborder="0"
							allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>

					<div class="footer-video-chi-duong">
						<div class="row">
							<div class="col-xl-3 col-xs-6 col-sm-6">
								<div class="one-content-chi-duong chi-duong-1">
									<div class="time-move"><span class="time-chu-to">5</span> <span class="phut">phút</span></div>
									<div class="type-move">Di chuyển ra cao tốc <br />Hà Nội - Hải Phòng</div>
								</div>
							</div>

							<div class="col-xl-3 col-xs-6 col-sm-6">
								<div class="one-content-chi-duong chi-duong-2 ">
									<div class="time-move"><span class="time-chu-to">10-20</span> <span class="phut">phút</span></div>
									<div class="type-move">Vào trung tâm thủ đô như <br />Hồ Gươm, Nhà hát Lớn Hà Nội</div>
								</div>
							</div>

							<div class="col-xl-3 col-xs-6 col-sm-6">
								<div class="one-content-chi-duong chi-duong-3">
									<div class="time-move"><span class="time-chu-to">5</span> <span class="phut">phút</span></div>
									<div class="type-move">Di chuyển ra <br />Aeon Long Biên</div>
								</div>
							</div>

							<div class="col-xl-3 col-xs-6 col-sm-6">
								<div class="one-content-chi-duong chi-duong-4">
									<div class="time-move"><span class="time-chu-to">30</span> <span class="phut">phút</span></div>
									<div class="type-move">Di chuyển đến sân bay <br />Quốc tế Nội Bài</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end section 6 -->

		<!-- begin section 7	 -->
		<div class="row section-7">
			<div class="col-md-12">
				<div class="content-section content-section-7 w-100-120">
					<div class="row">
						<div class="col-xl-4 col-md-12">
							<!-- content-form -->
							<div class="content-form bottom">
								<!-- <div class="tham-nha" style="background-image: url('images/tham-nha.png')"></div> -->
								<!-- <div class="vinhomes">VINHOMES</div> -->
								<!-- <div class="laithu" style="width: 100%"><span>LÁI XE VINFAST</div> -->

								<form>
									<div class="text-center">
										<h1 class="text-center" style="text-align: center; color:#FFFFFF">
											VINHOMES OCEAN PARK
										</h1>
										<h4 class="text-center" style="text-align: center; letter-spacing: 5px; color:#20295A">ƯU ĐÃI MỚI
											NHẤT</h4>
									</div>
									<div class="text-form">
										<div class="form-group">
											<label for="hoten"></label>
											<input type="text" class="form-control" id="hoten1" aria-describedby="hotenHelp"
												placeholder="Họ tên">
											<small id="hotenHelp" class="form-text text-muted"></small>
										</div>
										<div class="form-group">
											<label for="sdt"></label>
											<input type="phone" class="form-control" id="sdt1" aria-describedby="sdtHelp"
												placeholder="Số điện thoại">
											<small id="sdtHelp" class="form-text text-muted"></small>
										</div>
										<div class="form-group">
											<label for="hoten"></label>
											<input type="email" class="form-control" id="email1" aria-describedby="emailHelp"
												placeholder="Email">
											<small id="emailHelp" class="form-text text-muted"></small>
										</div>
									</div>

									<div class="radio-form">
										<p class="title-radio-form title-section-form">căn hộ bạn muốn sở hữu</p>
										<div class="row">
											<div class="col-md-6">
												<label class="container radio-title">Căn hộ 1 PN
													<input type="radio" name="radio">
													<span class="checkmark checkmark-bottom"></span>
												</label>
											</div>
											<div class="col-md-6">
												<label class="container radio-title">Căn hộ 2 PN + 1
													<input type="radio" name="radio">
													<span class="checkmark checkmark-bottom"></span>
												</label>
											</div>
										</div>

										<div class="row">
											<div class="col-md-6">
												<label class="container radio-title">Căn hộ 1 PN + 1
													<input type="radio" name="radio">
													<span class="checkmark checkmark-bottom"></span>
												</label>
											</div>
											<div class="col-md-6">
												<label class="container radio-title">Căn hộ 3 PN
													<input type="radio" name="radio">
													<span class="checkmark checkmark-bottom"></span>
												</label>
											</div>
										</div>
									</div>

									<!-- <p class="title-section-form dang-ky-lai-xe">đăng ký trải nghiệm xe VinFast </p>

									<div class="form-group combobox">
										<select class="form-control combobox-bottom">
											<option>Thứ 6 - 06/03/2020</option>
											<option>Thứ 7 - 07/03/2020</option>
											<option>CN - 08/03/2020</option>
											<option>Thứ 6 - 13/03/2020</option>
											<option>Thứ 7 - 14/03/2020</option>
											<option>CN - 15/03/2020</option>
											<option>Thứ 6 - 20/03/2020</option>
											<option>Thứ 7 - 21/03/2020</option>
											<option>Thứ 6 - 27/03/2020</option>
											<option>Thứ 7 - 28/03/2020</option>
											<option>CN - 29/03/2020</option>
											<option>Không đăng ký lái thử xe</option>
										</select>
										<small id="emailHelp" class="form-text text-muted"></small>
									</div> -->
									<div class="button-submit">
										<button type="submit" class="btn btn-primary btn-dang-ky-chuong-trinh">Đăng ký chương trình</button>
									</div>

								</form>
							</div><!-- end content form -->
						</div>

						<div class="col-xl-8 col-md-12">
							<div class="content-has-car" style="margin-top: 150px;">
								<div class="title-section font-size-32">
									<p>MUA NHÀ VINHOMES NHẬN VOUCHER</p>
									<p>MUA XE VINFAST LÊN ĐẾN</p>
									<h1 style="font-size:52px; font-weight: bold ">200 TRIỆU</h1>
									<!-- <p>THĂM NHÀ VINHOMES - LÁI THỬ XE VINFAST</p> -->
								</div>

								<div class="despcription">
									<div class="one-description">
										<p class="title-one-description">
											TẶNG KÈM HỌC PHÍ LÊN ĐẾN 3 NĂM TẠI VINSCHOOL
										</p>
										<p>Chỉ dành riêng cho khách hàng mua căn hộ Sapphire 2 - Vinhomes Ocean Park</p>
									</div>
									

									<div class="image-car" style="background-image: url('images/car.png')">

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end section 7  -->
		<div class="row" id="footer" style="background-image: url('images/background-footer.png')">
			<div class="col-md-12">
				<div class="row background-has-opacity">
					<div class="col-xl-4 col-sm-6">
						<div class="container-image-footer" style="background-image: url('images/logo-footer.png');"></div>
					</div>
					<div class="col-xl-4 col-sm-6">
						<div class="all-infor cl-2">
							<div class="on-info top-all-infor">
								<p class="title-footer lien-he">Liên hệ</p>
							</div>
							<div class="on-info dia-chi">
								<p class="title-footer">Địa chỉ</p>
								<p class="description-footer">Gia lâm, Hà nội</p>
							</div>
							<div class="on-info hotline">
								<p class="title-footer">Hotline</p>
								<p class="description-footer">0888 049 669</p>
							</div>
							<div class="on-info email">
								<p class="title-footer">Email</p>
								<p class="description-footer">bds@vinhomes.vn</p>
							</div>
							<div class="on-info dia-chi">
								<p class="title-footer">Địa chỉ giao dịch</p>
								<p class="description-footer">TT GD BDS Times City – TL13, Tầng B1, <br />TTTM Vincom Megamall, 458 Minh
									Khai, <br /> P. Vĩnh Tuy, Q. Hai Bà Trưng, Hà Nội.</p>
								<p class="phone">02439328328</p>
							</div>
						</div>
					</div>

					<div class="col-xl-4 col-sm-6">
						<div class="all-infor cl-3">
							<div class="on-info top-all-infor">
								<p class="title-footer lien-he">chú ý</p>
							</div>
							<div class="on-info">
								<p>* Thông tin, hình ảnh, các tiện ích trên website <br /> chỉ mang tính chất tương đối và có thể được
									điều chỉnh <br />theo quyết định của Chủ đầu tư tại từng thời điểm đảm bảo phù hợp quy hoạch và thực
									tế thi công Dự án. <br />Các thông tin, cam kết chính thức sẽ được quy định cụ thể tại Hợp đồng mua
									bán. <br />Việc quản lý, vận hành và kinh doanh của khu đô thị sẽ theo quy định của Ban quản lý.</p>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	</body>

<!-- <script src="vendor/grid-gallery-master/js/grid-gallery.min.js"></script>
<script>
    gridGallery({
      selector: ".dark",
      darkMode: true
    });
    gridGallery({
      selector: "#horizontal",
      layout: "horizontal"
    });
    gridGallery({
      selector: "#square",
      layout: "square"
    });
    gridGallery({
      selector: "#gap",
      gaplength: 10
    });
    gridGallery({
      selector: "#heightWidth",
      rowHeight: 180,
      columnWidth: 280
    });
    </script> -->

</html>